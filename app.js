const imgTab = document.querySelectorAll('.img')
const tabLenght = imgTab.length;

// Création des cecles a la voléé
let cerclesToInsert = "";
for (let i = 0; i < tabLenght; i++) {
    cerclesToInsert += "<button><img src=\"./img/circle.svg\" alt=\"cercle\" id=" + i + " class=\"cercles\"></i></button>"
    //event listener cercles
    document.body.addEventListener('click', function (event) {
        if (event.target === document.getElementById(i)) {
            let img = document.getElementsByClassName('slider')
            img[0].classList.replace('slider', 'hidden')
            imgTab[i].classList.replace('hidden', 'slider')
        }
    })

    //ajout du data Attribut au éléments du tablea
    imgTab[i].setAttribute('data-num', i);
}
document.getElementById('cicle').innerHTML = cerclesToInsert;

//event listener bouton gauche
document.body.addEventListener('click', function (event) {
    if (event.target === document.getElementById("chevron-left")) {
        let newImage = "";
        const immage = document.getElementsByClassName('slider');
        let numImage = immage[0].getAttribute('data-num')
        immage[0].classList.replace('slider', 'hidden')
        if (parseInt(numImage) - 1 >= 0) {
            newImage = imgTab[parseInt(numImage) - 1];
        } else {
            newImage = imgTab[parseInt(tabLenght) - 1];
        }
        newImage.classList.replace('hidden', 'slider')
    }
})

//event listenet bouton droit
document.body.addEventListener('click', function (event) {
    if (event.target === document.getElementById("chevron-right")) {
        let newImage = "";
        const immage = document.getElementsByClassName('slider');
        let numImage = immage[0].getAttribute('data-num')
        immage[0].classList.replace('slider', 'hidden')
        if (parseInt(numImage) + 1 <= tabLenght - 1) {
            newImage = imgTab[parseInt(numImage) + 1];
        } else {
            newImage = imgTab[0];
        }
        newImage.classList.replace('hidden', 'slider')
    }
})